# 3. 3D Printing
This week's agenda included a comprehensive exploration of 3D printing technology. The primary objective is to execute the printing of our Module , which demands a foundational understanding of PrusaSlicer. 

## The Printer:
In our lab, we are fortunate to have access to the cutting-edge [Prusa i3 MK3/S/S+ 3D printer](https://www.prusa3d.com/category/original-prusa-i3-mk3s/). This sophisticated piece of engineering excellence empowers us to bring our creative concepts to life with precision and accuracy. To complement our hands-on experience with this remarkable machine, we have a dedicated [GitLab resource](https://gitlab.com/fablab-ulb/enseignements/fabzero/3d-print/-/blob/main/3D-printers.md) at our disposal. This resource serves as a comprehensive guide, providing valuable insights and step-by-step instructions on how to effectively operate and maximize the potential of the Prusa i3 MK3/S/S+. 


![](images/module03/IMG_6413.jpg){width=50%}

At home, I have a [Flashforge Adventurer 3 3D Printer](https://www.flashforgeshop.com/product/flashforge-adventurer-3-3d-printer?cID=34). It's a great machine to work with, and it's really affordable. It's easy to connect to my device with Wi-Fi without the need for a flashcard. I've been using it to print all sorts of things, from toys and figurines to practical household items.
It has a build volume of 150 x 150 x 150 mm and can print with a variety of materials, including PLA, ABS, and PETG. 

![](images/module03/IMG_4829.JPG){width=30%}
![](images/module03/IMG_4834.JPG){width=30%}

## Slicer: 
At the FabLab, we use the PrusaSlicer software to prepare our 3D prints for the Prusa 3D printers we have on hand. Upon launching PrusaSlicer for the first time, you'll be prompted to specify the printers you intend to use with the software. Since we only use Prusa printers at the FabLab, I selected the models we have available: MK3, equipped with a 0.4mm nozzle. 

![](images/module03/prusaslicer.PNG){width=40%}

for the Flashforge slicer I use the slicer that came with the machine flashprint its very simple easy to use with 2 mode expert and basic using the 0.4 mm nozzle it can give you the option to make the bild faster so this is what i use to test the model then i create the final one with the stander with bigger layer high 

![](images/module03/flashpoint.PNG){width=40%}

## Upload and slice 
Export as STL: To export the model as an STL file, navigate to the "File" menu, choose "Export," and select "Export as STL." Provide a name and location for the STL file and save it.

![](images/module03/export%20stl.PNG){width=40%}

After saving the model with both OpenScad and FreeCAD we upload the model to the slicer then we will see the option to add support for the part that are in the air or my cause problem in the printing the system do this automatically. 

![](images/module03/slice%20parcer.PNG){width=50%}
![](images/module03/slice%20plachprint.PNG){width=50%}

Once the model is positioned, navigate to the support generation settings. PrusaSlicer offers various support settings that can be adjusted based on your specific needs. You can typically find these under the "Supports" tab.

![](images/module03/support.PNG){width=50%}

PrusaSlicer provides options for different types of supports such as tree supports or standard supports. Adjust the support settings like density, interface layers, or support pillar resolution based on the complexity of the model and the required support.
## Printing 
exporting the G-code to the printer and it will do the rest starting by heating the head and the plate , calibrate the position the start printing.
* Note: clean the plate before using it 

![](images/module03/export.PNG)

* note you will find the STL file here [1](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/my%20first%20openscad.stl?ref_type=heads), [2](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/second%20openscad.stl?ref_type=heads), [3](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/kik-BodyPocket002.stl?ref_type=heads)

## Testing :
The first test that I did was for the first design and when printing it I found out that the measurement that I used was small.

![](images/module03/IMG_6613.jpg){width=50%}

Then after that I forgot to put support on it so the upper layer collapse. So from now on I need to check if additional support is needed.

![](images/module03/IMG_6617.jpg){width=50%}


The second part was printed as I wanted it was a small part, so it did not take much time and I printed 6.

![](images/module03/IMG_6618.jpg){width=50%}

The second part was the flex join assignment, so I did the test of the bending it went up to 80°.

![](images/module03/IMG_6623.jpg){width=50%}
![](images/module03/ezgif.com-resize.gif){width=50%}

The holes were also tested, and they fit well for the 5mm hole.

![](images/module03/IMG_6622.jpg){width=50%}

## Final result.
For the final result, I was experimenting with a part I designed earlier. Additionally, I created a cylinder that serves as a holder, assembling the parts together like LEGO bricks. This design allows for the creation of a shape that changes its position when pressure is applied to the center of the flex. 

![](images/module03/ezgif.com-video-to-gif.gif){width=50%}

