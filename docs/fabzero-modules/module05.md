# 5. Group Dynamics and Final Project

This week, I began shaping my final project concept and initiated my familiarity with the documentation procedures. 

* note the first day of this week i was absent but i undrestod that they creata a problem tree and a solution tree 
# the problem that I see Is waste management problem :  
```
       Problem Tree - Waste Pollution
               [Root Problem]
                  Waste Pollution
              /        |         \
       [Causes] [Causes] [Causes] [Causes]
         /         |          |          \
    Inadequate   Excessive   Lack of    Population
    Disposal     Plastic     Recycling   Growth
   Practices     Usage       Facilities

               |            |           |
       [Effects]   [Effects]   [Effects]   [Effects]
         /         |          |          \
 Environmental   Health     Resource    Biodiversity
 Degradation    Hazards    Depletion      Loss
```

# the solution: 
```
       Solution Tree - Waste Pollution
              [Root Solutions]
            Waste Pollution Mitigation
              /        |         \
       [Solutions] [Solutions] [Solutions]
         /         |          |          \
    Inadequate   Excessive   Lack of    Population
    Disposal     Plastic     Recycling   Growth
   Practices     Usage       Facilities

             |            |           |
       [Solutions]   [Solutions]   [Solutions]   [Solutions]
         /         |          |          \
Promote Proper Advocate for Establish    Implement
Waste         Reduced      Recycling    Family Planning
Segregation   Plastic      Centers      and Reproductive
Raise Public  Usage        In Communities Health Programs
Awareness     Alternatives Develop and Educate the Public
                          Enforce Recycling on Sustainable
                          Policies     Living Practices

               |            |           |
       [Solutions]   [Solutions]   [Solutions]
         /         |          |          \
Improve      Promote      Establish
Healthcare   Safe Waste   Protected Areas
Access and   Handling and Wildlife
Services     Disposal     Corridors
Monitor and  Monitor and  Conduct
Address      Address      Biodiversity
Health Risks  Risks        Conservation
```

# Group project 

In the group formation process, I found it intriguing to witness everyone bringing objects representing modern-day problems. It was eye-opening to see the variety of concerns and perspectives. Personally, I was particularly drawn to environmental issues, focusing on items like plastic waste, water usage, and electricity consumption. Exploring these topics made me realize the urgency of addressing sustainability challenges in our daily lives.

I brought a piece of garbage to emphasize the pressing waste problem in the world. I highlighted that if we don't address this issue now, it could become an insurmountable problem in the future.
On that day, we took the opportunity to examine and discuss the items everyone had brought with them. We had a chance to inquire about their individual ideas and perspectives. Afterward, we formed a cohesive group that best represented a common and shared concept or idea.


![](images/module05/IMG_6463.jpg){width=50%}

Following that, the professor instructed us to jot down the issues and challenges that sprang to mind in relation to the overarching theme, "Make the invisible visible." This theme sparked various evident problem areas, such as "How to visually represent air pollution" and "How to visually represent water pollution." Additionally, we unearthed a mix of ideas, with some being less certain in their relevance, such as "How to substitute sand," while others were notably intriguing, like "How to effectively eliminate noise pollution."

* NOTE: 
For the final project documentation, the professor established a group GitLab repository to facilitate the organization and tracking of our ideas and progress for the final project. You can access our GitLab repository at [https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/groups/group-06](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/groups/group-06).