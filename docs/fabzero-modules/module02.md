# 2. Computer-Aided Design

This week marked the beginning of our journey into the world of 3D modeling software. We were introduced to two distinct software tools: OpenSCAD and FreeCAD, each with its unique approach to 3D design.

OpenSCAD and FreeCAD are two distinct open-source software applications used for 3D modeling and computer-aided design (CAD). They cater to different design and modeling needs and have unique features:

1. **OpenSCAD**:

   1. OpenSCAD is a script-based 3D modeling software. Instead of using a traditional graphical interface to manipulate objects, users create 3D models by writing scripts in a programming language-like syntax.
   2. It's known for its parametric modeling capabilities, making it an excellent choice for users who want precise control over their designs. Parameters can be easily adjusted to modify the model.
   3. OpenSCAD is often preferred by those who are comfortable with coding or scripting and who require a high level of control over their designs.
   to downlowd openscad use this [link](https://openscad.org/)
   This is the [cheatsheet](https://openscad.org/cheatsheet/) to help us with openscad 

2. **FreeCAD**:
   1. FreeCAD is a more traditional parametric 3D CAD modeler with a graphical user interface (GUI). It allows users to create and manipulate 3D models by selecting and modifying objects from a visual interface.
   2. It's particularly useful for engineering and architectural projects. FreeCAD supports the creation of complex parametric models, making it a versatile tool for a wide range of applications.
   3. FreeCAD is an excellent choice for users who prefer a more intuitive and visual approach to 3D modeling, and it's often used in engineering and architecture fields.
   To downlowd FreeCAD  use this [link](https://www.freecad.org/)

## Openscad example : 
These examples showcase the basic syntax and concepts in OpenSCAD:

**Example 1: Creating a Cube**

```openscad
// Example 1: Creating a cube
cube([10, 10, 10]);
```

This code generates a cube with dimensions 10x10x10 units. ![](images/module02/cube.PNG)

**Example 2: Creating a Cylinder**

```openscad
// Example 2: Creating a cylinder
cylinder(h = 10, d = 10, $fn = 100);
```

This code creates a cylinder with a height of 10 units, a diameter of 10 units, and smooth 100 facets.

![](images/module02/cylender.PNG)

**Example 3: Combining Objects**

```openscad
// Example 3: Combining objects (cube and sphere)
difference() {
    cube([20, 20, 10]);
    translate([10, 10, 0])
        sphere(5);
}
```

This code demonstrates how to combine objects. It creates a cube and then subtracts a smaller sphere from it using the `difference()` function.

![](images/module02/combien%20cube%20with%20spher.PNG)

**Example 4: Module and Parameters**

```openscad
// Example 4: Using modules and parameters
module myBox(length, width, height) {
    cube([length, width, height]);
}

myBox(15, 10, 5);
```

This example defines a module `myBox` that takes parameters for length, width, and height. Then, it uses this module to create a box with specific dimensions.

![](images/module02/module.PNG)

**Example 5: Loops and Arrays**

```openscad
// Example 5: Using loops and arrays
for (x = [0:10:90]) {
    translate([x, 0, 0])
        cube([5, 5, 5]);
}
```

![](images/module02/loop.PNG)

## First Design:

1. ## Using OpenScad: 
this work was based on Morgan Tonglet work 
// File : mod2_flexilink001.scad

// Author : Morgan Tonglet

// Date : 26 février 2023

// License : Creative Commons Attribution-ShareAlike 4.0 International [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/)


click [here](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/my%20first%20openscad.scad?ref_type=heads) to go to the code source and [here](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/my%20first%20openscad.stl?ref_type=heads) for the STL file 
```openscad
// File : my first openscad.scad

// Author : sif eddine boughris

// Date : 20/10/2023
// License : 3d openscad fablab © 2023 by Sif eddine Boughris is licensed under CC BY-NC 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/



$fn = 200;

// Attachment piece dimensions
attachment_length = 10;
attachment_width = 10;
attachment_thickness = 2;

// Piece with holes dimensions
piece_height = 10;
N_holes = 4;
radius = 2.5;
distance = 8;
edge_thickness = 2;

// Define the attachment module
module attachment() {
    translate([-attachment_length / 2, -attachment_width / 2, 0])
        cube([attachment_length, attachment_width, attachment_thickness]);
}

// Define the main piece with holes
module main_piece() {
    difference() {
        union() {
            // Create the piece with holes
            translate([0, 0, piece_height / 2])
                difference() {
                    hull() {
                        for (i = [1:N_holes])
                            translate([(i - 1) * distance, 0, 0])
                                cylinder(h = piece_height, r = radius, $fn = 100);
                        translate([(N_holes - 1) * distance, 0, 0])
                            cylinder(h = piece_height, r = radius + edge_thickness, $fn = 100);
                    }
                    for (i = [1:N_holes])
                        translate([(i - 1) * distance, 0, 0])
                            cylinder(h = piece_height + 1, r = radius - 1, $fn = 100);
                }
        }
        // Attach the piece to the head
        translate([0, 0, piece_height])
            attachment();
    }
}

// Display the complete structure
main_piece();

```
![](images/module02/my%20pice.PNG)

click [here](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/my%20first%20openscad.scad?ref_type=heads) to go to the code source and [here](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/second%20openscad.stl?ref_type=heads) for the STL file 
```openscad
// File : second openscad.scad

// Author : sif eddine boughris

// Date : 20/10/2023
// License : 3d openscad fablab © 2023 by Sif eddine Boughris is licensed under CC BY-NC 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/

$fn = 100;

nHoles = 1;
rOut = 4;
rInn = 2.5;
thick = 3;
holeSep = 0;

linkLen = 70;
linkWid = 1.5;
slotLen = 6 * rOut;

module holeClip(xPos, yPos, zPos) {
    difference() {
        union() {
            cylinder(h = thick , r = rOut +1, $fn = 100);
            translate([xPos, yPos, zPos])
                cylinder(h = thick , r = rInn, $fn = 100);
        }
        translate([xPos, yPos, zPos])
            sphere(r = rOut, $fn = 100);
    }
}

module filledSlot(xPos, yPos, zPos) {
    hull() {
        translate([xPos, yPos, zPos])
            cylinder(h = thick, r = rOut, $fn = 100);
        translate([xPos + slotLen, yPos, zPos])
            cylinder(h = thick, r = rOut, $fn = 100);
    }
}

module link() {
    dist = (nHoles - 1) * (2 * rOut + holeSep);
    hull() {
        translate([dist, linkWid / 2, thick / 2])
            rotate([90, 0, 0])
            cylinder(r = thick / 2, h = linkWid, $fn = 100);
        translate([dist + linkLen, linkWid / 2, thick / 2])
            rotate([90, 0, 0])
            cylinder(r = thick / 2, h = linkWid, $fn = 100);
    }
}

module assembly() {
    difference() {
        difference() {
            union() {
                link();
                holeClip(2 * rOut * (nHoles - 1) + holeSep * (nHoles - 1), 0, 0);
                filledSlot(2 * rOut * (nHoles - 1) + holeSep * (nHoles - 1) + linkLen, 0, 0);
            }
            for (i = [1:nHoles]) {
                translate([(2 * rOut + holeSep) * (i - 1), 0, 0])
                    cylinder(h = thick, r = rInn, $fn = 100);
            }
        }
        hull() {
            translate([(2 * rOut + holeSep) * (nHoles - 1) + linkLen, 0, 0])
                cylinder(h = thick, r = rInn, $fn = 100);
            translate([(2 * rOut + holeSep) * (nHoles - 1) + linkLen + slotLen, 0, 0])
                cylinder(h = thick, r = rInn, $fn = 100);
        }
    }
}

assembly();


```

![](images/module02/second%20design.PNG)

2. ## Using FreeCAD : 
TO create a flex shape I try to create a catapult with a basic design the final shape that I came up with was this :  
// File : kik.FCStd

// Author : sif eddine boughris

// Date : 26/10/2023
// License : 3d freecad model © 2023 by Sif eddine Boughris is licensed under CC BY-NC 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/


![](images/module02/freecad%20design.PNG)

FOR the design file click [here](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/kik.FCStd?ref_type=heads) and for the STL file click [here](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module02/kik-BodyPocket002.stl?ref_type=heads)

## Creative Commons License
[Creative Commons](https://creativecommons.org/share-your-work/cclicenses/) (CC) is a non-profit organization that provides a range of free, legally-sound tools and licenses to give creators a simple and standardized way to grant copyright permissions for their creative work. These licenses allow creators to share their work with others while specifying the conditions under which it can be used, remixed, or shared by others. 

Including a license and proper credits is crucial in open-source projects to clarify how others can use and distribute the code. In the case of FreeCAD and OpenSCAD, licensing ensures that users understand the permissions and restrictions when utilizing the software or its code for their own projects.

To include a license in code, you typically create a separate file (commonly named LICENSE or COPYING) in the project root directory. Within this file, you detail the type of license (e.g., MIT, GPL, Apache) and the terms under which others can use, modify, and distribute your code. Additionally, you can add license headers to individual code files to explicitly state the licensing terms for each specific file. This is usually done at the beginning of the file.

The main Creative Commons licenses are as follows:

| License                                 | Symbol | Description                                                                                      |
|-----------------------------------------|--------|--------------------------------------------------------------------------------------------------|
| **CC BY (Attribution)**                 | ![CC BY](https://licensebuttons.net/l/by/3.0/88x31.png)  | Allows distribution, remixing, and adaptation for any purpose, provided proper attribution is given.            |
| **CC BY-SA (Attribution-ShareAlike)**   | ![CC BY-SA](https://licensebuttons.net/l/by-sa/3.0/88x31.png)  | Similar to CC BY, but derivative works must also be shared under the same terms.                               |
| **CC BY-ND (Attribution-NoDerivatives)** | ![CC BY-ND](https://licensebuttons.net/l/by-nd/3.0/88x31.png)  | Allows redistribution, including for commercial purposes, as long as the work is not modified and proper attribution is given. |
| **CC BY-NC (Attribution-NonCommercial)** | ![CC BY-NC](https://licensebuttons.net/l/by-nc/3.0/88x31.png)  | Permits remixing, adaptation, and distribution for non-commercial purposes, with proper attribution.                 |
| **CC BY-NC-SA (Attribution-NonCommercial-ShareAlike)** | ![CC BY-NC-SA](https://licensebuttons.net/l/by-nc-sa/3.0/88x31.png)  | Similar to CC BY-NC, but derivative works must also be shared under the same terms.                             |
| **CC BY-NC-ND (Attribution-NonCommercial-NoDerivatives)** | ![CC BY-NC-ND](https://licensebuttons.net/l/by-nc-nd/3.0/88x31.png)  | Allows redistribution for non-commercial purposes, as long as the work is not modified and proper attribution is given. |




