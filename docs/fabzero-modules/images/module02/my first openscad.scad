// File : my first openscad.scad

// Author : sif eddine boughris

// Date : 20/10/2023
// License : 3d openscad fablab © 2023 by Sif eddine Boughris is licensed under CC BY-NC 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/
$fn = 200;

// Attachment piece dimensions
attachment_length = 10;
attachment_width = 10;
attachment_thickness = 2;

// Piece with holes dimensions
piece_height = 10;
N_holes = 4;
radius = 4;
distance = 8;
edge_thickness = 2;

// Define the attachment module
module attachment() {
    translate([-attachment_length / 2, -attachment_width / 2, 0])
        cube([attachment_length, attachment_width, attachment_thickness]);
}

// Define the main piece with holes
module main_piece() {
    difference() {
        union() {
            // Create the piece with holes
            translate([0, 0, piece_height / 2])
                difference() {
                    hull() {
                        for (i = [1:N_holes])
                            translate([(i - 1) * distance, 0, 0])
                                cylinder(h = piece_height, r = radius, $fn = 100);
                        translate([(N_holes - 1) * distance, 0, 0])
                            cylinder(h = piece_height, r = radius + edge_thickness, $fn = 100);
                    }
                    for (i = [1:N_holes])
                        translate([(i - 1) * distance, 0, 0])
                            cylinder(h = piece_height + 1, r = radius - 1, $fn = 100);
                }
        }
        // Attach the piece to the head
        translate([0, 0, piece_height])
            attachment();
    }
}

// Display the complete structure
main_piece();
