// File : second openscad.scad

// Author : sif eddine boughris

// Date : 20/10/2023
// License : 3d openscad fablab © 2023 by Sif eddine Boughris is licensed under CC BY-NC 4.0. To view a copy of this license, visit http://creativecommons.org/licenses/by-nc/4.0/


$fn = 100;

nHoles = 1;
rOut = 4;
rInn = 2.5;
thick = 3;
holeSep = 0;

linkLen = 70;
linkWid = 1.5;
slotLen = 6 * rOut;

module holeClip(xPos, yPos, zPos) {
    difference() {
        union() {
            cylinder(h = thick , r = rOut +1, $fn = 100);
            translate([xPos, yPos, zPos])
                cylinder(h = thick , r = rInn, $fn = 100);
        }
        translate([xPos, yPos, zPos])
            sphere(r = rOut, $fn = 100);
    }
}

module filledSlot(xPos, yPos, zPos) {
    hull() {
        translate([xPos, yPos, zPos])
            cylinder(h = thick, r = rOut, $fn = 100);
        translate([xPos + slotLen, yPos, zPos])
            cylinder(h = thick, r = rOut, $fn = 100);
    }
}

module link() {
    dist = (nHoles - 1) * (2 * rOut + holeSep);
    hull() {
        translate([dist, linkWid / 2, thick / 2])
            rotate([90, 0, 0])
            cylinder(r = thick / 2, h = linkWid, $fn = 100);
        translate([dist + linkLen, linkWid / 2, thick / 2])
            rotate([90, 0, 0])
            cylinder(r = thick / 2, h = linkWid, $fn = 100);
    }
}

module assembly() {
    difference() {
        difference() {
            union() {
                link();
                holeClip(2 * rOut * (nHoles - 1) + holeSep * (nHoles - 1), 0, 0);
                filledSlot(2 * rOut * (nHoles - 1) + holeSep * (nHoles - 1) + linkLen, 0, 0);
            }
            for (i = [1:nHoles]) {
                translate([(2 * rOut + holeSep) * (i - 1), 0, 0])
                    cylinder(h = thick, r = rInn, $fn = 100);
            }
        }
        hull() {
            translate([(2 * rOut + holeSep) * (nHoles - 1) + linkLen, 0, 0])
                cylinder(h = thick, r = rInn, $fn = 100);
            translate([(2 * rOut + holeSep) * (nHoles - 1) + linkLen + slotLen, 0, 0])
                cylinder(h = thick, r = rInn, $fn = 100);
        }
    }
}

assembly();
