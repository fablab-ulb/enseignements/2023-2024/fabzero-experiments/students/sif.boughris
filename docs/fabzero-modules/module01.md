# 1. Gestion de projet et documentation

In this specific section, my primary focus revolved around the initial setup of my GitLab profile, connecting it seamlessly with Visual Studio Code (VS Code), and emphasizing the significance of documenting our findings.
I carried out the project using VS Code, which served as my comprehensive development environment. It allowed me to work seamlessly on the project without the need for additional tools.



## Install Git:
![](images/module01/Git-Logo-2Color.png ){width=40%}

The first step of my journey involved installing Git, a critical tool for version control and collaboration. Git enables the efficient tracking of code changes and facilitates seamless teamwork. It serves as the foundation for maintaining a well-organized and synchronized project repository. 

To read about git click [here](https://git-scm.com/).

To download the git directly click [here](https://git-scm.com/downloads).


## Install Visual Studio Code (VS Code):
![](images/module01/Visual_Studio_Code_1.35_icon.svg.png){width=30%}\\

Following the installation of Git, I proceeded to install Visual Studio Code (VS Code), a robust and versatile code editor. VS Code became the central hub for my development activities, offering a user-friendly interface and a wide array of extensions that streamlined the coding and project management process.

To find the official page of VS Code click [here](https://code.visualstudio.com/).

To download the VS Code click [here](https://code.visualstudio.com/Download)




## Creating the Repository:
![FABLAB-REPO](images/module01/fablab-platform.PNG){width=60%}

With my development environment in place. The repository was established as a template for each student to follow, ensuring a consistent structure and a standardized workflow for our project. This uniform approach simplified collaboration and made it easier to track progress.
The repository that was assigned to me is [link](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris).

![](images/module01/sif-eddine-fablab-platform.PNG){width=50%}




## Creating an SSH Key for the Repository:
To enhance the security of the project repository, I generated an SSH key. This key allowed for secure and authenticated access to the repository, safeguarding against unauthorized changes and ensuring data integrity. The SSH key became a crucial element in our workflow.
# Generating a new SSH key:
You have the option to create a fresh SSH key on your computer. Once the key is generated, you can upload the public part of the key to your GitHub.com account. This will allow you to use SSH for authentication when performing Git operations:
1. Open Git Bash.
2. Paste the text below :
```
ssh-keygen -t rsa -b 4096 -C "your_email@example.com" 
```
3. Choose a path where you want to save your .ssh file or use the one proposed by the system then press enter.
4. Enter passphrase this paraphrase will be asked each time you commit to the repository 
![](images/module01/ssh.PNG){width=50%}
5. go to the ssh file path and (ex : /c/Users/Sif eddine/.ssh/id_rsa.pub) and open the file .pub 
6. copy the ssh serial number.
7. Go to the GitLab setting on your profile then ssh and create a new ssh key and past the previous created ssh key 

# Link VS Code with Gitlab : 
To link Visual Studio Code (VS Code) with GitLab using an extension, you'll want to follow these general steps:

1. Install GitLab Extension: GitLab provides an official VS Code extension that makes it easier to work with GitLab repositories. Search for "GitLab" in the VS Code Extensions Marketplace and install the "GitLab Workflow" or a similar extension. Make sure it's an extension associated with GitLab.

![](images/module01/gitlab%20extention.PNG){width=50%}

2. Configure Your GitLab Account:
   on the side bare of the VS Code you will find a symbol of the GitLab click on it then it will give you the option to link it with you GitLab account, the GitLab extension should help manage your GitLab connection. 

3. Clone or Open Repository: You can either clone a GitLab repository using the URL
 on the sidebar on the source control symbol ![](images/module01/source%20control.PNG) .
 
 * After that choose clone :

 ![](images/module01/clone.PNG){width=50%}{center}

 * choose the GitLab:

 ![](images/module01/clone%20from%20git%20lab.PNG){width=50%}

 * choose the repository 

 ![](images/module01/clone%20represatory.PNG){width=50%}

 * choose the ssh key link :

 ![](images/module01/clone%20using%20the%20ssh%20or%20the%20git.PNG){width=50%}

 * it will ask you for the folder to clone in :

 ![](images/module01/distination%20clone.PNG){width=50%}


4. Use GitLab Features: With the GitLab extension installed and configured, you can access GitLab features directly from VS Code, such as creating merge requests, reviewing issues, and more.

5. Push and Pull: You can push your changes to GitLab and pull updates from the remote repository using the Git features integrated into VS Code.
6. After any saved changes in the cloned repo you can find that it ask you if you want to commit and sync the new changes. 

![](images/module01/commit%20and%20sync.PNG){width=30%} 
![](images/module01/sync.PNG){width=30%}


## compressing images: 
Before adding images to your GitLab repository, it's a good practice to optimize and compress them to reduce file sizes and improve loading times. One effective tool for image compression is [iloveimg.com](https://www.iloveimg.com/compress-image).

By using iloveimg.com's image compression service, you can significantly decrease the file sizes of your images without compromising their quality. This ensures that your GitLab repository remains efficient and doesn't consume unnecessary storage space or bandwidth. Compressed images also contribute to faster loading times, enhancing the overall user experience for those accessing your project. So, it's a valuable step to optimize your images with iloveimg.com before integrating them into your GitLab repository, promoting an efficient and responsive project environment.

One of other ways to reduce the size of image is to use ImageMagick:

![](images/module01/Image%20magick.PNG){width=60%}

To resize an image to a specific file size to like 5 MB, you'll need to adjust the compression level rather than simply specifying a dimension. This is because the file size of an image is determined by its dimensions and compression level.

This is a step-by-step guide on how to use ImageMagick to resize an image to a file size of 5 MB:

1. Install ImageMagick: Before you can use ImageMagick, you'll need to install it on your system. You can find installation instructions for different operating systems on the ImageMagick website ([https://www.imagemagick.org/](https://www.imagemagick.org/): [https://www.imagemagick.org/](https://www.imagemagick.org/)).

![](images/module01/Image%20magick%20version.PNG){width=60%}

2. Open the Terminal: Once ImageMagick is installed, open a terminal window. This is where you'll execute ImageMagick commands.

3. Change to the Image Directory: Navigate to the directory containing the image you want to resize using the `cd` command. For example, if your image is named `image.jpg` and it's located in the `images` folder, you would use the following command:

```
cd images
```

4. Resize the Image with Quality Adjustment: Use the `mogrify -quality ` command to resize the image while adjusting the quality. The basic syntax is as follows:

```
mogrify -quality 60% *.jpg
```

## Markdown: 
Markdown is a lightweight markup language that is commonly used for formatting plain text documents. It was designed to be easy to write and easy to read, with a simple and intuitive syntax. Markdown is often used for creating documents that will be converted to HTML or other formats for web publishing.

In Markdown, you can format text using simple characters and symbols to indicate headings, lists, emphasis (bold and italic), links, images, and more. It is widely used in documentation, README files, and online communication platforms like GitHub, GitLab, and Stack Overflow.

Here are a few examples of Markdown formatting:

- **Headers**:
  ```
  # This is a level 1 header
  ## This is a level 2 header
  ```

- **Lists**:
  ```
    Item 1
    Item 2
    Item 3
  ```

- **Emphasis**:
  ```
  *Italic text* or _Italic text_
  **Bold text** or __Bold text__
  ```

- **Links**:
  ```
  [Link Text](https://www.example.com)
  ```

- **Images**:
  ```
  ![Alt Text](image.jpg)
  ```

useful link for [Markdown](https://www.markdownguide.org/cheat-sheet/)

## Project management principles: 
Project management principles are fundamental concepts and guidelines that serve as the foundation for effectively planning, executing, and controlling projects. These principles are widely recognized in the field of project management and provide a framework for achieving project success. Here are some key project management principles:


1. **As-You-Work Documentation:**
   * This principle emphasizes the importance of documenting your work as you progress through a project. It serves as a personal record of your steps, issues encountered, and how you resolved them.
   * Documentation helps with knowledge retention, troubleshooting, and sharing insights with others.

2. **Spiral Development:**
   * The spiral development approach encourages iterative progress by writing draft versions of a project and gradually refining them.
   * This iterative approach can be effective for complex projects, allowing you to build upon previous work and incorporate improvements over time.

3. **Modular and Hierarchical Planning:**
   * Breaking a project into smaller, manageable tasks and organizing them logically is a sound strategy. It helps maintain focus and ensures that the most critical components are addressed first.
   * This approach simplifies project management by dividing it into more manageable units.

4. **Triage:**
   * Prioritizing the most critical aspects of a project is crucial for effective time and resource management. Triage ensures that high-impact tasks receive immediate attention.
   * It aligns well with focusing on what matters most and achieving key objectives.

5. **Supply-Side Time Management (Not Your Preference):**
   * This approach organizes tasks by time allocation rather than specific tasks. However, it seems that this approach doesn't work well for you, as it can lead to distractions and a lack of focus.

6. **Parkinson's Law:**
   * Parkinson's Law highlights that tasks often expand to fill the time allotted to them. This is a common observation and emphasizes the need for efficient time management.

7. **Hofstadter's Law:**
   * Hofstadter's Law suggests that projects typically take longer than expected. Acknowledging this principle can help in setting more realistic timelines and expectations.

It's essential to adapt and combine these principles to fit your specific project needs and work style. For instance, you can use As-You-Work Documentation in conjunction with Triage and Spiral Development to create a comprehensive and organized workflow that suits your goals and helps you stay on track. The key is to find a balance that maximizes your productivity and efficiency while delivering high-quality results.

## Issues : 
In GitLab, you can add issues to your project to track and manage tasks, bugs, or feature requests. However, GitLab primarily uses the Markdown file format for issues, not CSV. To add issues in GitLab, follow these steps:

1. **Navigate to Your Project**: Open your GitLab project in your web browser.

2. **Go to the "Issues" Section**: Click on the "Issues" tab in your project's menu. This is where you'll manage and create issues.


3. **Click on "Import Issues"**: Look for an "Import Issues" button or a similar option. This varies depending on your GitLab version. Click it to start the import process.

4. **Upload the CSV File**: Select and upload the CSV file you prepared.

5. **Map CSV Columns to GitLab Fields**: GitLab will ask you to map the columns in your CSV to GitLab's issue fields. Ensure that the fields align correctly.

6. **Start the Import**: Once you've mapped the fields, start the import process. GitLab will create issues in your project based on the CSV data.

![](images/module01/issues.PNG){width=50%}

