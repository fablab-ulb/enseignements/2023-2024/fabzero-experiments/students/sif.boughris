# 4. 3D Printing Repair shop & laze cuter 

## electonic 
The assistant provided us with a kit containing an interrupter and a lamp, assigning us the task of identifying the faulty component within the circuit. To accomplish this, we meticulously assessed each element of the circuit using a multimeter. After thorough testing, it was determined that the malfunctioning component in my kit was the LED and a wire that was cut under the plastic. 

it was a fun session we saw different component like 


1. **Resistor**: Resistors limit the flow of current in a circuit, and they are used to control voltage levels, set bias points in transistors, and protect LEDs from excessive current.

2. **Capacitor**: Capacitors store and release electrical energy. They are used for smoothing power supplies, filtering signals, and timing in oscillators.

3. **Diode**: Diodes allow current to flow in only one direction. They are used in rectifiers to convert AC to DC, voltage clamping, and signal protection.

4. **Transistor**: Transistors amplify or switch electronic signals. They are fundamental in amplifiers, digital logic circuits, and voltage regulation.

5. **LED (Light Emitting Diode)**: LEDs emit light when a current passes through them. They are used for indicators, displays, and in optoelectronic applications.

6. **Inductor**: Inductors store energy in a magnetic field. They are used in filters, transformers, and energy storage devices.



## Remake parts and 3d print it 
for this part we were asked to bring something that is broken with us, and we need to change the broken peace by modeling it and 3d print it. 

I had a USB flash drive I lost the lid of, so using and a vernier caliper I was able to get the measurement and create a model using FreeCAD

![](images/module03/usb%20lid.PNG)

the first one I forgot to add support, so the shape was deformed, and the hole was a little small had that's because I did not take into consideration the printer head, so I had to do Somme adjustment to it by resizing the file using the first printed design  

![](images/module04/IMG_6550.jpg){width=50%}

* final print using Prusa i3 MK3:

![](images/module04/IMG_6551.jpg){width=50%}

* using my printer :

![](images/module04/IMG_6562.jpg){width=50%}

![](){width=50%}

## Lazer cutter : 
for This part I worked with the Epilog Fusion Pro 32 machine.
The Epilog Fusion Pro 32 is a popular laser engraving and cutting machine. To get started with this machine,we must follow these general steps:

1. **Safety Precautions:**
   - Ensure you are wearing appropriate safety gear, such as safety glasses.
   - Make sure the machine is in a well-ventilated area, and you have fire safety equipment nearby.

2. **Power On:**
   - Locate the power switch on the machine and turn it on. This will provide power to the laser and control system.

3. **Prepare Your Material:**
   - Place the material you want to engrave or cut on the laser bed. Make sure it's flat and properly aligned.

4. **Laser Focus:**
   - Use the built-in focusing tool to adjust the laser's focus based on the thickness of your material. This is crucial for achieving accurate results.

5. **Job Setup:**
   - Use the machine's control panel or software to set up your job. This may include importing your design or adjusting settings like speed, power, and resolution.

   ![](images/module04/IMG_6529.jpg){width=50%}
   

6. **Print or Send the Job:**
   - Once your job is set up, you can either print it directly from the machine's control panel or send it to the machine from your computer using dedicated software.

7. **Execute the Job:**
   - After sending the job, start the laser engraving or cutting process. The machine will move the laser head according to your design.

8. **Monitor the Job:**
   - Keep an eye on the machine while it's working to ensure everything is going as expected.

9. **Completion and Cooling:**
   - After the job is done, allow the machine to cool down if necessary. Some materials and engraving processes may generate heat.

10. **Remove Finished Work:**
    - Carefully remove your finished piece from the laser bed.

11. **Power Off:**
    - Turn off the power switch on the machine when you're finished using it.

# Incscape:
instructions for an Inkscape procedure:

1. **Configure Page Layout and Dimensions**: Begin by setting up the page grid and specifying the size of your working canvas.

2. **Sketch the Box Sides**: Using nominal dimensions, draw the outlines of the box sides, ensuring precision in your measurements.

3. **Utilize the Path Tool for Tabs and Slots**: Employ the path tool to create tabs and slots that will allow for secure assembly of the box components.

4. **Define Line Color and Thickness for Laser Cutting**: Select the appropriate line color and adjust line thickness settings, ensuring they are compatible with the requirements of your laser cutting machine.

5. **Save the File and Prepare for Printing**: Save your design file, and then proceed to print it using the settings appropriate for your specific laser cutting equipment.



# Class assignment  : 
The instructions required us to create a box, divide it with equidistant lines, and then perform a laser cut. We designed the box, performed the laser cut to create smaller boxes, and assembled them together. Afterward, we calculated the total space at the end and divided it by the number of lines. The final result indicated a spacing of 1.1 millimeters between the lines.

![](images/module04/IMG_6532.jpg){width=50%}
![](images/module04/test%20recangle%20lasersaure%20.jpg){width=50%}

## Assignment: 
I designed a construction kit using a rectangular piece of cardboard, which was 4mm thick. I incorporated slots on the sides of the cardboard piece to ensure that it could be easily connected. Additionally, I added crosswise lines extending from the edges of the shape to enhance stability. In the center of the shape, I included a circular feature. These lines were not cut all the way through but were scored, allowing the cardboard to bend in specific areas. This design facilitates various assembly possibilities while considering the laser cutter's kerf, ensuring precise and flexible construction options.

Using Inkscape to draw the shape and color-coding the middle line is a clever approach to categorize and distinguish different parameters for the laser cutter. This method allows for better organization and communication with the laser cutter, ensuring that the scoring and cutting processes are accurately executed. Color-coding is a useful visual technique to convey specific instructions and optimize the laser cutting process.

![](images/module04/assigment.PNG){width=50%} 

After conducting tests with the laser cutter, I determined the optimal parameters for my project. I chose the following settings:

**Black Lines:**
- Speed: 40
- Power: 60
- Frequency: 100

**Red Lines:**
- Speed: 60
- Power: 15
- Frequency: 100

Here's the information in a table format for clarity:

| Line Color | Speed | Power | Frequency |
|------------|-------|-------|-----------|
| Black      | 40    | 60    | 100       |
| Red        | 60    | 15    | 100       |

These settings resulted in the desired outcome when working with the laser cutter. 

![](images/module04/IMG_6571.jpg){width=50%}
![](images/module04/IMG_6572.jpg){width=50%}


this was the result after printing

![](images/module04/IMG_6574.jpg){width=50%}

* The final product can be assembled into different shapes, allowing for creative and versatile configurations. It offers flexibility and encourages experimentation in creating various designs.
The link for the SVG file is [here](https://gitlab.com/fablab-ulb/enseignements/2023-2024/fabzero-experiments/students/sif.boughris/-/blob/main/docs/fabzero-modules/images/module04/drawing.svg?ref_type=heads)

![](images/module04/IMG_6575.jpg){width=30%} 
![](images/module04/IMG_6576.jpg){width=30%} 
![](images/module04/IMG_6577.jpg){width=30%} 
![](images/module04/IMG_6579.jpg){width=30%} 

