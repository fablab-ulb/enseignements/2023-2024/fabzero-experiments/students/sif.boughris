## Foreword

Hello! welcome to my student blog for the [2023-2024 ULB class "How To Make (almost) Any Experiments Using Digital Fabrication"](https://fablab-ulb.gitlab.io/enseignements/2023-2024/fabzero-experiments/class-website/).

## About me

![](images/avatar-photo2.jpg)

Hey there! I'm Sif Eddine Boughris, an enthusiastic student in computer science and engineering at the Université Libre de Bruxelles (ULB). This platform is my digital canvas, where I aim to capture my learning journey, delving into the fascinating realms of 3D printing, laser machines, and tinkering with development boards like Raspberry Pi and Arduino. It's all about exploring these cutting-edge technologies and immersing myself in the dynamic FabLab culture.

As you navigate through the sections above, you'll gain insights into my projects and ideas. I invite you to join me on this exciting voyage of discovery, where I'll document my successes and challenges in the quest for knowledge and innovation.

Visit this website to see my work!

## My background

I was born in 1994 in Algeria, a place where my journey of learning and self-discovery began. As I pursued my education in my home country, I found myself drawn to the fascinating world of computer science and engineering. It was there, in the vibrant educational landscape of Algeria, that I laid the foundation for my future endeavors.

In September 2019, I embarked on a new chapter of my life as I traveled to Belgium to further my education. The Université Libre de Bruxelles (ULB) became my academic home, where I embarked on a challenging yet exciting path to complete my master's degree. This transition marked a significant turning point in my life, offering me the opportunity to explore new horizons, meet diverse individuals, and broaden my knowledge in the field I am truly passionate about.

My journey has been shaped by the cultural diversity and academic excellence that Belgium has to offer. It's here that I've had the privilege to immerse myself in a global community of learners, to share my experiences and perspectives, and to engage with cutting-edge technologies, such as 3D printing, laser machines, and development boards like Raspberry Pi and Arduino.

As I continue to study and grow in this new environment, I look forward to the challenges and opportunities that lie ahead, aiming to contribute my knowledge and skills to the ever-evolving field of computer science and engineering.

## Previous work

Prior to my academic journey in Belgium, I gained valuable real-world experience through various professional endeavors. One of my most recent and rewarding experiences was an internship with a company called Tomra. During this internship, I had the opportunity to immerse myself in the dynamic world of technology and innovation.

At Tomra, I was tasked with exploring and experimenting with a cutting-edge technology known as CORAL TPU (Tensor Processing Unit). This experience allowed me to delve into the intricacies of this innovative hardware and its applications. I worked alongside a talented team of professionals, contributing my knowledge and skills while also learning from their expertise.

![CORAL TPU](images/CORAL-EDGE-TPUB-TOP.jpg)

This internship not only broadened my technical proficiency but also provided me with a glimpse into the practical applications of the theoretical knowledge I had gained during my academic pursuits. It was a significant step in my journey, where I had the chance to bridge the gap between classroom learning and its real-world implementation.

My time at Tomra further fueled my passion for technology and instilled in me a sense of purpose in pursuing a career that combines my academic background with hands-on experience. It's experiences like these that continue to shape my aspirations and drive my commitment to excellence in the field of computer science and engineering.

If you're interested in exploring my previous projects and work, I invite you to visit my GitHub page at [github](https://github.com/sif-eddine-boughris). There, you'll find a collection of repositories showcasing my coding and development endeavors, offering insights into my technical skills and contributions.

Additionally, you can connect with me on LinkedIn [page](https://www.linkedin.com/in/sifeddine-boughris/) to stay updated on my professional journey, discover more about my experiences, and engage in meaningful discussions related to technology, computer science, and engineering. Feel free to reach out, connect, and explore the exciting projects and ideas that I've had the privilege of working on.

